const express = require('express');
const app = express();
const fs = require('fs');
const path = require('path');
const outputPath = path.join(__dirname, 'files');
app.use(express.json());

try {
    if (!fs.existsSync(outputPath)) {
        fs.mkdirSync(outputPath);
    }
} catch (err) {
    throw err;
}

function requestLogger(req, res, next) {
    res.on('finish', function() {
        const message = `${req.method} request to ${req.url}, status code ${res.statusCode}`;
        console.log(message);
        fs.appendFileSync(path.join(__dirname, 'debug.log'), `\n${message}`);
    });
    next();
}

function errorHandler({statusCode, message}, req, res, next) {
    res.status(statusCode).json({ message });
    next(message);
}

function errorLogger(err, req, res, next) {
    res.on('finish', function() {
        console.error(err);
        fs.appendFileSync(path.join(__dirname, 'debug.log'), `\n${err}`);
    });
}

function writeFile(filename, content) {
    const extensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];
    const fileExtension = getFileExtension(filename);
    if(extensions.includes(fileExtension)) {
        return fs.writeFileSync(path.join(outputPath, filename), content);
    } else {
        throw new Error('Invalid extension');
    }
}

function getFiles() {
    return fs.readdirSync(outputPath);
}

function getFile(filename) {
    const content = fs.readFileSync(path.join(outputPath, filename), 'utf8');
    const uploadedDate = fs.statSync(path.join(outputPath, filename)).birthtime;
    const extension = getFileExtension(filename);
    return {
        filename,
        content,
        extension,
        uploadedDate
    };
}

function deleteFile(filename) {
    return fs.unlinkSync(path.join(outputPath, filename));
}

function isFileExist(filename) {
    const files = getFiles();
    return files.includes(filename);
}

function checkEmptyParams(params) {
    if (!params.hasOwnProperty('filename')) return 'filename';
    if (!params.hasOwnProperty('content')) return 'content';
    for (let key in params) {
        if (!params[key]) return key;
    }
}

function getFileExtension(filename) {
    return path.extname(filename).split('.')[1];
}

app.get('/', (req, res) => {
    res.send('GET request to homepage');
});

app.post('/api/files', requestLogger, async (req, res, next) => {
    try {
        const { filename, content } = req.body;
        const emptyParam = checkEmptyParams(req.body);
        if (isFileExist(filename)) {
            next({ statusCode: 400, message: 'File already exists' });
        } else if (emptyParam) {
            next({ statusCode: 400, message: `Please specify '${emptyParam}' parameter` });
        } else {
            await writeFile(filename, content);
            res.json({ message: 'File created successfully' });
        }
    } catch (error) {
        if(error.message) {
            next({ statusCode: 400, message: error.message });
        } else {
            next({ statusCode: 500, message: 'Server error' });
        }
    }
});

app.get('/api/files', requestLogger, async (req, res, next) => {
    try {
        const files = await getFiles();
        if(!files.length) {
            next({ statusCode: 400, message: 'Client error' });
        } else {
            res.json({ message: 'Success', files });
        }
    } catch (error) {
        next({ statusCode: 500, message: 'Server error' });
    }
});

app.get('/api/files/:filename', requestLogger, async (req, res, next) => {
    try {
        const filename = req.params.filename;
        if (!isFileExist(filename)) {
            next({ statusCode: 400, message: `No file with '${filename}' filename found` });
        } else {
            const file = await getFile(filename);
            res.json({ message: 'Success', ...file });
        }
    } catch (error) {
        next({ statusCode: 500, message: 'Server error' });
    }
});

app.put('/api/files/:filename', requestLogger, async (req, res, next) => {
    try {
        const filename = req.params.filename;
        const emptyParam = checkEmptyParams({ filename, ...req.body });
        if (!isFileExist(filename)) {
            next({ statusCode: 400, message: `No file with '${filename}' filename found` });
        } else if (emptyParam) {
            next({ statusCode: 400, message: `Please specify 'content' parameter` });
        } else {
            await writeFile(filename, req.body.content);
            res.json({ message: 'Success' });
        }
    } catch (error) {
        next({ statusCode: 500, message: 'Server error' });
    }
});

app.delete('/api/files/:filename', requestLogger, async (req, res, next) => {
    try {
        const filename = req.params.filename;
        if (!isFileExist(filename)) {
            next({ statusCode: 400, message: `No file with '${filename}' filename found` });
        } else {
            await deleteFile(filename, req.body.content);
            res.json({ message: 'Success' });
        }
    } catch (error) {
        next({ statusCode: 500, message: 'Server error' });
    }
});

app.use(requestLogger, errorHandler, errorLogger);

app.listen( 8080, () => {
    console.log('Server has been started')
});